-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 07, 2014 at 07:51 AM
-- Server version: 5.5.27
-- PHP Version: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `laporhendi`
--

-- --------------------------------------------------------

--
-- Table structure for table `berita`
--

CREATE TABLE IF NOT EXISTS `berita` (
  `id_berita` int(11) NOT NULL AUTO_INCREMENT,
  `tgl_buat` datetime NOT NULL,
  `status_publish` enum('draft','publish') NOT NULL,
  `username` varchar(200) NOT NULL,
  `judul` varchar(100) NOT NULL,
  `artikel` text NOT NULL,
  `gambar` varchar(200) NOT NULL,
  PRIMARY KEY (`id_berita`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `berita`
--

INSERT INTO `berita` (`id_berita`, `tgl_buat`, `status_publish`, `username`, `judul`, `artikel`, `gambar`) VALUES
(1, '2014-06-06 00:00:00', 'publish', 'aliyyil', 'Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit...', '\r\n\r\nVestibulum vehicula mi ac ante dignissim congue. Nulla ac augue sed sem accumsan fringilla vitae et velit. Quisque ornare convallis imperdiet. Morbi dignissim nibh eu cursus pellentesque. Suspendisse commodo quam et eros mollis, ac tincidunt odio feugiat. Nunc at sem nec sem ullamcorper condimentum. Aliquam erat volutpat. In eu lacus fermentum, consequat mauris eu, sollicitudin eros. Nullam tempus neque a justo varius malesuada consequat ac magna. Curabitur non libero in massa hendrerit tempus porttitor quis libero. Pellentesque convallis auctor est a aliquet. Aliquam eleifend varius augue, sit amet ultricies turpis consequat blandit. Suspendisse mattis justo in urna pharetra volutpat. Praesent ac tellus lectus. Suspendisse tincidunt tristique nisi et dapibus.\r\n\r\nMauris eleifend semper augue, a consectetur diam pellentesque quis. Duis sagittis ipsum vel arcu luctus, sit amet interdum magna accumsan. Sed ac urna sagittis, dictum leo a, lobortis diam. In ac eros vel ipsum porttitor luctus a vel eros. Nulla lacus augue, dictum id tempus id, adipiscing vel ante. Donec eget lectus massa. Nunc lorem lorem, bibendum non tortor vel, convallis imperdiet orci. Interdum et malesuada fames ac ante ipsum primis in faucibus.\r\n\r\nMaecenas molestie libero non massa congue, non aliquam dui ullamcorper. Nam molestie tortor ut velit tristique, a laoreet quam luctus. Sed sagittis porttitor imperdiet. Nunc turpis magna, rutrum eu vehicula quis, scelerisque eget turpis. Quisque in ante et urna hendrerit vehicula ac ut tortor. Maecenas porttitor odio quis aliquam blandit. Donec ornare porttitor ultrices. Cras rutrum velit sed facilisis euismod. Nunc elit lacus, viverra non orci sed, posuere ornare tellus. Praesent ante purus, molestie non commodo nec, scelerisque nec nisi. Integer imperdiet lectus vel adipiscing pellentesque. Donec magna sem, imperdiet nec dui at, venenatis pharetra erat. Nam egestas quam nec nisl sagittis fermentum. Curabitur pharetra ultricies metus, ac congue nisl ultrices quis.\r\n\r\nMauris et odio nisi. Duis enim velit, pulvinar et condimentum ut, venenatis quis ligula. Nunc vestibulum massa quis ultrices vestibulum. Duis massa diam, fringilla ut sollicitudin non, luctus a velit. Integer laoreet, quam non tristique sodales, urna nisi porta leo, in mattis nisi nunc vel nulla. Duis tortor diam, pretium sed viverra ultrices, tincidunt ut tortor. Phasellus blandit nisl eget massa ullamcorper auctor. Sed vitae tortor in tellus condimentum ullamcorper ut a mauris. Ut luctus ultrices rutrum. Nulla condimentum vel diam ut dapibus. Maecenas luctus condimentum ipsum, eget vehicula libero hendrerit in. Nam ultricies varius sem non molestie. Nam sit amet porta lectus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Donec vulputate magna vel ligula auctor fringilla. Sed adipiscing augue pellentesque scelerisque luctus.\r\n\r\nNunc suscipit nisi ut metus molestie, non varius sem malesuada. Aenean faucibus metus et purus eleifend porttitor. In vitae odio odio. Donec quis velit sed orci vestibulum hendrerit. Vivamus nec dictum purus. Praesent sagittis laoreet ante quis congue. Phasellus varius ipsum elit, vel convallis mi tempor ut. Sed nisi urna, tempus ac lorem eget, aliquet lacinia leo. Maecenas feugiat aliquet lectus, ultricies feugiat lectus euismod in. ', 'apr-14-ipoh-hor-fun-nocal-1920x1440.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `kategori`
--

CREATE TABLE IF NOT EXISTS `kategori` (
  `id_kategori` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `desc` text NOT NULL,
  `deleted` int(11) NOT NULL,
  PRIMARY KEY (`id_kategori`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `kategori`
--

INSERT INTO `kategori` (`id_kategori`, `name`, `desc`, `deleted`) VALUES
(1, 'Jalan Rusak', 'Jalan Rusak', 0);

-- --------------------------------------------------------

--
-- Table structure for table `laporan`
--

CREATE TABLE IF NOT EXISTS `laporan` (
  `id_lapor` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(200) NOT NULL,
  `id_kategori` int(11) NOT NULL,
  `judul` varchar(200) NOT NULL,
  `deskripsi` text NOT NULL,
  `alamat` text NOT NULL,
  `gambar` text NOT NULL,
  `lon` varchar(50) NOT NULL,
  `lat` varchar(50) NOT NULL,
  `tgl_publish` datetime NOT NULL,
  `verified` enum('0','1') NOT NULL DEFAULT '0',
  `verified_by` varchar(200) NOT NULL,
  PRIMARY KEY (`id_lapor`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `laporan`
--

INSERT INTO `laporan` (`id_lapor`, `username`, `id_kategori`, `judul`, `deskripsi`, `alamat`, `gambar`, `lon`, `lat`, `tgl_publish`, `verified`, `verified_by`) VALUES
(1, 'user1', 1, 'judul', 'deskripsi', 'alamat', 'gb', '', '', '0000-00-00 00:00:00', '0', 'feri'),
(5, 'user2', 1, 'judul', 'deskripsi', 'alamat', 'gb', '', '', '0000-00-00 00:00:00', '1', 'feri'),
(6, 'user2', 1, 'judul', 'deskripsi', 'alamat', 'gb', '', '', '0000-00-00 00:00:00', '1', 'feri'),
(7, 'user2', 1, 'judul', 'deskripsi', 'alamat', 'gb', '', '', '0000-00-00 00:00:00', '1', 'feri'),
(8, 'aliyyil.m@gmail.com', 0, 'Tes Judul', 'Deskripsi Deskripsi Deskripsi Deskripsi Deskripsi Deskripsi Deskripsi Deskripsi Deskripsi Deskripsi Deskripsi Deskripsi Deskripsi Deskripsi ', 'Test Alamat', '', '110.4353131', '-7.0602148', '2014-05-30 00:32:21', '1', ''),
(9, 'aliyyil.m@gmail.com', 0, 'Tes Judul', 'Deskripsi Deskripsi Deskripsi Deskripsi Deskripsi Deskripsi Deskripsi Deskripsi Deskripsi Deskripsi Deskripsi Deskripsi Deskripsi Deskripsi Deskripsi Deskripsi Deskripsi Deskripsi ', 'Test Alamat', '', '110.43726325035095', '-7.0592627934823025', '2014-05-30 00:32:57', '1', 'AAL');

-- --------------------------------------------------------

--
-- Table structure for table `likes`
--

CREATE TABLE IF NOT EXISTS `likes` (
  `id_likes` int(11) NOT NULL AUTO_INCREMENT,
  `tgl_likes` datetime NOT NULL,
  `id_laporan` int(11) NOT NULL,
  `id_user` varchar(200) NOT NULL,
  PRIMARY KEY (`id_likes`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=27 ;

--
-- Dumping data for table `likes`
--

INSERT INTO `likes` (`id_likes`, `tgl_likes`, `id_laporan`, `id_user`) VALUES
(8, '2014-05-30 01:53:52', 9, 'user1'),
(7, '2014-05-30 01:53:51', 8, 'user1'),
(9, '2014-05-30 01:53:54', 1, 'user1'),
(14, '2014-05-31 06:40:39', 9, 'aliyyil'),
(11, '2014-05-30 01:54:10', 2, 'aliyyil.m@gmail.com'),
(26, '2014-06-07 04:55:52', 9, 'aliyyil.m@gmail.com'),
(23, '2014-06-07 04:52:18', 8, 'aliyyil.m@gmail.com'),
(19, '2014-06-01 02:10:16', 8, 'aliyyil');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `username` varchar(200) NOT NULL,
  `nama` varchar(200) NOT NULL,
  `password` varchar(200) NOT NULL,
  `no_ktp` varchar(20) NOT NULL,
  `foto_profil` text NOT NULL,
  `jenis_kelamin` enum('L','P') NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `alamat` varchar(300) NOT NULL,
  `no_hp` varchar(20) NOT NULL,
  `twitter` varchar(50) NOT NULL,
  `level` int(11) NOT NULL DEFAULT '1' COMMENT '1 -> user -> 88 ->admin',
  `deleted` enum('0','1') NOT NULL DEFAULT '0',
  `is_twitter` enum('0','1') NOT NULL DEFAULT '0',
  `autentikasi_twitter` text NOT NULL,
  PRIMARY KEY (`username`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`username`, `nama`, `password`, `no_ktp`, `foto_profil`, `jenis_kelamin`, `tanggal_lahir`, `alamat`, `no_hp`, `twitter`, `level`, `deleted`, `is_twitter`, `autentikasi_twitter`) VALUES
('aliyyil.m@gmail.com', 'Aliyyil Musthofa', '3b536fbe6621eb436ffa0a382fbf3e70', '', 'thumb.jpg', 'L', '0000-00-00', '', '', '', 1, '0', '0', ''),
('user1', 'User Satu', '3b536fbe6621eb436ffa0a382fbf3e70', '', '', 'L', '0000-00-00', '', '', '', 1, '0', '0', ''),
('user2', 'User Dua', '3b536fbe6621eb436ffa0a382fbf3e70', '', '', 'L', '0000-00-00', '', '', '', 1, '0', '0', ''),
('budi@budi.com', 'budiman', '3b536fbe6621eb436ffa0a382fbf3e70', '', '', 'L', '0000-00-00', '', '', '', 1, '0', '0', ''),
('aliyyil', 'aliyyil musthofa', '', '', 'http://pbs.twimg.com/profile_images/472255777671872512/LDlWdAQP.png', 'L', '0000-00-00', '', '', 'aliyyil', 1, '0', '1', 'W4WWsMOLd12o6SUnMzYiyM8H8i48FZSKyTA5wrIWRxuLq||61405144-xto4e8TPYXH6nqblaRmITpioYRYYLbMYBmoc6CZ6D||61405144'),
('stepisendana', 'stepisendana stepisendana stepisendana stepisendana', '3b536fbe6621eb436ffa0a382fbf3e70', '', 'http://pbs.twimg.com/profile_images/378800000742918951/b867e2b508e82fc388dc7087d0ba737a.jpeg', 'L', '0000-00-00', '', '', 'stepisendana', 1, '0', '0', '437K0KgkOlvqUOHANvgz1zTpZoCustWMIrMjmpDJDZxHY||178367510-VINkZGfATb0xuPfTDcgvYesDXkKoGWU2N3HVoVRW||178367510'),
('gagatbeka@gagat.com', 'gagatbeka', 'e152c63f91debe873bb63f96c8c491fe', '', '', 'L', '0000-00-00', '', '', '', 1, '0', '0', ''),
('admin', 'administrator', '3b536fbe6621eb436ffa0a382fbf3e70', '', '', 'L', '0000-00-00', '', '', '', 88, '0', '0', '');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
